#TODO: 
#      SPEEDUP: LUT

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random
import json
import argparse
import os.path

resize=0.3

def resized(frame):
	global resize
	ret=cv2.resize(frame,dsize=(0,0),fx=resize,fy=resize)
	return ret

def trackVideo(tracked,videoname,out_video):
	global resize

	cap=cv2.VideoCapture(videoname)
	num_frames=cap.get(cv2.CAP_PROP_FRAME_COUNT)
	ret,frame=cap.read()

	if out_video:
		fourcc = cv2.VideoWriter_fourcc(*'X264')
		visfilename=os.path.dirname(out_video)
		if len(visfilename)>0:
			visfilename=visfilename+"/visualization"+os.path.basename(out_video)
		else:
			visfilename="visulization"+out_video
		outvis = cv2.VideoWriter(visfilename,fourcc, cap.get(cv2.CAP_PROP_FPS), (frame.shape[1],frame.shape[0]))

	auto_step=False
	t=0
	while (ret):
		frame_copy=np.copy(frame)
		curr_back=[]
		for tr in tracked:
			if tr["name"] in ["bluebox","ergo-rail-back","ergo-rail-front","ergo-wheel-center","ergo-back-bottom"]:
				continue
			if tr["prediction_type"][t]=="manual":
				color=(255,255,200)
			elif tr["prediction_type"][t]=="exact":
				color=(255,255,0)
			elif tr["prediction_type"][t]=="nearest":
				color=(140,140,255)
			elif tr["prediction_type"][t]=="estimation1":
				color=(0,0,255)
			elif tr["prediction_type"][t]=="estimation2":
				color=(0,255,0)
			else:
				color=(10,10,30)
			p=tr["center_history"][t]
			p=(p[0],p[1])
			cv2.circle(frame_copy,p,int(3/resize),color,-1)

		for pixel in tracked[len(tracked)-1]["back_history"][t]:
			frame_copy[pixel[1],pixel[0],2]=255
			frame_copy[pixel[1],pixel[0]-1,2]=255
		if out_video:
			outvis.write(frame_copy)
		cv2.putText(frame_copy,'{:.1f}'.format(t/num_frames*100)+"%",(5,frame_copy.shape[0]-5),cv2.FONT_HERSHEY_SIMPLEX,3.0,(255,255,255),3)
		cv2.imshow("visualization",resized(frame_copy))
		if auto_step:
			c=cv2.waitKey(1)
		else:
			c=cv2.waitKey(0)
 		if c & 0xFF == ord('q'):
        		break
		elif c & 0xFF == ord('a'):
        		auto_step=not auto_step
		ret,frame=cap.read()
		t=t+1
	if out_video:
		outvis.release()


parser = argparse.ArgumentParser()
parser.add_argument("--input", help="input video file", required=True)
parser.add_argument("--out", help="output json file to visualize", default="markers.json")
parser.add_argument("--video", help="output video file")
args = parser.parse_args()

with open(args.out,"r") as fi:
	tracked=json.load(fi)
		
trackVideo(tracked,args.input,args.video)
with open(args.out,"w") as fo:
	json.dump(tracked,fo)
